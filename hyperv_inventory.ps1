﻿#List of Hyper-V nodes (hostnames)
$nodes = "hyperv00","hyperv01","hyperv03"

#path to create reports for each node
$reportpath = "D:\"

foreach ($hyperv in $nodes)
{
    Write-Host ("Report for node '"+$hyperv+"'... ") -NoNewline
    $csvpath = $reportpath+$hyperv+".csv"
    #check, if report exist
    if(Test-Path $csvpath)
    {
        Remove-Item $csvpath
    }

    $VMs = Get-VM -ComputerName $hyperv
    foreach ($VM in $VMs) 
    {
        $VMName = $VM.VMName
        $VMMemory = $VM.MemoryAssigned/1MB
        $CPUCount = $VM.ProcessorCount
        $VHDDisks = $VM.HardDrives
        #check, if VM Description exist
        Try
        {
            $VMDescription = $VM.Notes.Split(",")
            $Department = $VMDescription[0]
            $Owner = $VMDescription[1].Trim()
        }
        Catch
        {
            $Owner = ""
        }
        $vhdsize = 0
        $vhdused = 0
        foreach ($disk in $VHDDisks) 
        {
            $path = $disk.Path 
            $vhd = Get-VHD -ComputerName $hyperv -Path $path 
            $vhdsize += [math]::Round($vhd.Size/1GB)
            $vhdused += [math]::Round($vhd.FileSize/1GB)
        }
        #calculate disk usage
        Try
        {
            $vhdpercent = ($vhdused/$vhdsize).ToString("P")
        }
        Catch
        {
            $vhdpercent = 0
        }
        $row=[pscustomobject]@{
            #Cluster = $Cluster.Name
            VMName = $VMName
            VMMemory = $VMMemory
            CPUCount = $CPUCount
            HDDSize = $vhdsize
            HDDUSed = $vhdused
            HDDPercent = $vhdpercent
            Department = $Department
            Owner = $Owner
            #NetSwitch = $NetSwitch.SwitchName
            #NetMACAdd = $NetMacAdd.Address
            #HostName = $HVHost.Name
            #VMState = $HVHost.State
        } 
        #add row to report
        Export-Csv -InputObject $row -Path $csvpath -Append -NoTypeInformation
    }
    Write-Host ("completed. Report file - "+$csvpath)
}